//
//  AppDelegate.h
//  FoodApplication
//
//  Created by ITHS on 2016-03-14.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

