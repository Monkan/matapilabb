//
//  FoodTableViewController.m
//  FoodApplication
//
//  Created by ITHS on 2016-03-14.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import "FoodTableViewController.h"
#import "FoodDetailViewController.h"

@interface FoodTableViewController ()

@property (nonatomic) NSArray *searchResult;
@property (strong, nonatomic) NSString *foodNumber;
@property (strong, nonatomic) NSString *nutrientValues;
@end

@implementation FoodTableViewController

-(NSArray*)searchResult{
    if (!_searchResult) {
        _searchResult=[[NSArray alloc]init];
    }
    return _searchResult;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *s = [NSString stringWithFormat:@"http://matapi.se/foodstuff?query=%@&format=json&pretty=1", self.data];
    NSString *escaped = [s stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSURL *url = [NSURL URLWithString:escaped];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error){
            NSLog(@"Error: %@", error);
            return ;
        }
        NSError *jsonParseError = nil;
        
        NSArray *result = [NSJSONSerialization JSONObjectWithData:data
                                        options:kNilOptions error:&jsonParseError];
        self.searchResult = result;
        
        if(jsonParseError) {
            NSLog(@"Failed to parse data %@", jsonParseError);
            return;
        }
        NSLog(@"Result %@", result);
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.tableView reloadData];
        });
    }];
    [task resume];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  
    return self.searchResult.count;
    }


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSDictionary *item = self.searchResult[indexPath.row];
    self.foodNumber = item[@"number"];
    cell.textLabel.text = item[@"name"];
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UITableViewCell*)sender {
    // Get the new view controller using [segue destinationViewController].
    
    FoodDetailViewController *foodDetailViewController = [segue destinationViewController];
    foodDetailViewController.title = sender.textLabel.text;
    foodDetailViewController.foodNumber = self.foodNumber;
    
    
    
    // Pass the selected object to the new view controller.
}


@end
