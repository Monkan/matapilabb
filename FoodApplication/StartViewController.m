//
//  StartViewController.m
//  FoodApplication
//
//  Created by ITHS on 2016-03-15.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import "StartViewController.h"
#import "FoodTableViewController.h"

@interface StartViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *kcalIcon;
@property (nonatomic) UIDynamicAnimator *animator;
@property (strong, nonatomic) IBOutlet UITextField *searchText;

@end

@implementation StartViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    UIGravityBehavior *gravityBehaviour = [[UIGravityBehavior alloc] initWithItems:@[self.kcalIcon]];
    UICollisionBehavior *collisionBehaviour = [[UICollisionBehavior alloc] initWithItems:@[self.kcalIcon]];
    UIDynamicItemBehavior *propertiesBehaviour = [[UIDynamicItemBehavior alloc] initWithItems:@[self.kcalIcon]];
    propertiesBehaviour.elasticity = 0.75f;
    
    collisionBehaviour.translatesReferenceBoundsIntoBoundary = YES;
    
    [self.animator addBehavior:gravityBehaviour];
    [self.animator addBehavior:collisionBehaviour];
    [self.animator addBehavior:propertiesBehaviour];
    
    // Do any additional setup after loading the view.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
   
    FoodTableViewController *foodTableViewController = [segue destinationViewController];
    NSString *data = self.searchText.text;
    foodTableViewController.data= data;
  }

@end


