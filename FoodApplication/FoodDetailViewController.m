//
//  FoodDetailViewController.m
//  FoodApplication
//
//  Created by ITHS on 2016-03-14.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import "FoodDetailViewController.h"

@interface FoodDetailViewController ()

@property (weak, nonatomic) NSDictionary *nutritions;
@property (weak, nonatomic) NSString *energy;
@property (weak, nonatomic) NSString *protein;
@property (weak, nonatomic) NSString *fat;
@property (weak, nonatomic) NSString *vitC;
@property (weak, nonatomic) NSString *vitD;
@property(nonatomic, copy) NSArray *animImages;
@property(readonly) double doubleValueX;
@property(readonly) double doubleValueY;
@property(readonly) double sumTotal;



@property (weak, nonatomic) IBOutlet UILabel *energyText;
@property (weak, nonatomic) IBOutlet UILabel *proteinText;
@property (weak, nonatomic) IBOutlet UILabel *fatText;
@property (weak, nonatomic) IBOutlet UILabel *vitDText;
@property (weak, nonatomic) IBOutlet UILabel *vitCText;
@property (weak, nonatomic) IBOutlet UIImageView *animatedImage;

@end

@implementation FoodDetailViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.animImages = [NSArray arrayWithObjects:[UIImage imageNamed:@"Birthday Cake-50"],
                            [UIImage imageNamed:@"Caloric Energy-50"],
                            [UIImage imageNamed:@"Cherry-50"],
                            [UIImage imageNamed:@"Chicken-50"],
                            [UIImage imageNamed:@"Cow-50"],
                            [UIImage imageNamed:@"Crab-50"],
                            [UIImage imageNamed:@"Duck-50"],
                            [UIImage imageNamed:@"Hamburger-50"],
                            [UIImage imageNamed:@"Ice Cream Cone-50"],
                            [UIImage imageNamed:@"Pig-50"],
                            [UIImage imageNamed:@"Sheep-50"],
                            [UIImage imageNamed:@"Spaghetti-50"],
                            [UIImage imageNamed:@"Vegan Food-50"],
                            [UIImage imageNamed:@"Mushroom-50"],
                            nil];
    
    self.animatedImage.animationImages = self.animImages;
    self.animatedImage.animationDuration = 10;
    
    [self.animatedImage startAnimating];
    
    self.energyText.text = @" ";
    self.proteinText.text = @" ";
    self.fatText.text = @" ";
    self.vitDText.text = @" ";
    self.vitCText.text = @" ";
    
    
    NSString *s = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", self.foodNumber];
    NSString *escaped = [s stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSURL *url = [NSURL URLWithString:escaped];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error){
            NSLog(@"Error: %@", error);
            return ;
        }
        NSError *jsonParseError = nil;
        
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                options:kNilOptions error:&jsonParseError];
        
        
        if(jsonParseError)
        {
            NSLog(@"Failed to parse data %@", jsonParseError);
            return;
            
        }
        self.nutritions = result[@"nutrientValues"];
       
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.energy = result[@"nutrientValues"][@"energyKcal"];
            self.energyText.text = [NSString stringWithFormat:@"Energi %@ kcal.", self.energy];
            self.protein = result[@"nutrientValues"][@"protein"];
            self.proteinText.text = [NSString stringWithFormat:@"Protein %@ g.", self.protein];
            self.fat = result[@"nutrientValues"][@"fat"];
            self.fatText.text = [NSString stringWithFormat:@"Fett %@ g.", self.fat];
            self.vitC = result[@"nutrientValues"][@"vitaminC"];
            self.vitCText.text = [NSString stringWithFormat:@"Vitamin C %@ mg.", self.vitC];
            //self.vitD = result[@"nutrientValues"][@"vitaminD"];
            //self.vitDText.text = [NSString stringWithFormat:@"Totalvärde: %f g.", _sumTotal];
            
            NSString *proteinString = self.protein;
            _doubleValueX = [proteinString doubleValue];
           
            
            NSString *fatString = self.fat;
            _doubleValueY = [fatString doubleValue];
            
            _sumTotal = _doubleValueX + _doubleValueY;
            self.vitDText.text = [NSString stringWithFormat:@"Näringsvärde: %f g.", _sumTotal];
            

        });
    }];
    [task resume];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end


